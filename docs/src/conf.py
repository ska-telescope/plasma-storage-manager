# Configuration file for the Sphinx documentation builder.

import os
import subprocess

# Run doxygen if we're in RTD to generate the XML/HTML documentation from C++
read_the_docs_build = os.environ.get('READTHEDOCS', None) == 'True'
if read_the_docs_build:
    os.makedirs('../build/doxygen', exist_ok=True)
    subprocess.call('doxygen', cwd='..')

# -- Project information -----------------------------------------------------
project = 'ska-sdp-plasmastman'
copyright = '2020, Rodrigo Tobar'
author = 'Rodrigo Tobar'
with open('../../version.txt') as f:
    version = f.read().strip()
release = version

extensions = [
    'breathe'
]
breathe_projects = {
    'ska-sdp-plasmastman': '../build/doxygen/xml'
}
breathe_default_project = 'ska-sdp-plasmastman'

html_theme = 'sphinx_rtd_theme'
