API
===

Casacore classes
----------------

:cpp:class:`ska::plasma::PlasmaStMan`
and :cpp:class:`ska::plasma::PlasmaStManColumn`
are the two main classes
implementing the Storage Manager API
as mandated by casacore.

.. doxygenclass:: ska::plasma::PlasmaStMan
   :members:

.. doxygenclass:: ska::plasma::PlasmaStManColumn
   :members:

Plasma access
-------------

.. doxygenclass:: ska::plasma::PlasmaClient
   :members:

Data reading
------------

Internally,
data reading is organised in a hierarchy of the *Reader* classes,
each taking care of reading different Arrow objects.

.. doxygenclass:: ska::plasma::ArrowReader
   :members:

.. doxygenclass:: ska::plasma::TensorReader
   :members:

.. doxygenclass:: ska::plasma::TableReader
   :members:

Misc
----

.. doxygenclass:: ska::plasma::ObjectID
   :members:
