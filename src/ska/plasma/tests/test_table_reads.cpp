#include <algorithm>
#include <complex>
#include <thread>
#include <utility>
#include <vector>
#include <type_traits>

#include <gtest/gtest.h>
#include <arrow/builder.h>
#include <arrow/table.h>
#include <arrow/type_traits.h>
#include <arrow/io/memory.h>
#include <arrow/ipc/writer.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/tables/Tables/RefRows.h>

#include "common_assertions.h"
#include "ska/plasma/exceptions.h"
#include "ska/plasma/plasma_stman.h"
#include "ska/plasma/types.h"
#include "test_common.h"

namespace ska {
namespace plasma {

template <typename T>
using ArrayBuilder = typename arrow::CTypeTraits<T>::BuilderType;

template <typename T>
std::shared_ptr<arrow::Array> to_array(const std::vector<T> &values)
{
	std::shared_ptr<arrow::Array> array;
	ArrayBuilder<T> builder;
	builder.AppendValues(values);
	builder.Finish(&array);
	return array;
}

template <typename T>
std::shared_ptr<arrow::Array> to_array_from_complex(const std::vector<std::complex<T>> &values)
{
	auto real_builder = std::make_shared<ArrayBuilder<T>>();
	auto imag_builder = std::make_shared<ArrayBuilder<T>>();
	std::shared_ptr<arrow::Array> real;
	std::shared_ptr<arrow::Array> imag;
	for (const auto &val: values) {
		real_builder->Append(val.real());
		imag_builder->Append(val.imag());
	}
	real_builder->Finish(&real);
	imag_builder->Finish(&imag);
	return std::dynamic_pointer_cast<arrow::Array>(
		std::make_shared<arrow::StructArray>(
			get_table_type<std::complex<T>>(), values.size(),
			std::vector<std::shared_ptr<arrow::Array>>{real, imag}
		)
	);
}

template <>
std::shared_ptr<arrow::Array> to_array<Int64>(const std::vector<Int64> &values)
{
	std::vector<int64_t> values_as_int64t {values.begin(), values.end()};
	return to_array<int64_t>(values_as_int64t);
}

template <>
std::shared_ptr<arrow::Array> to_array<Complex>(const std::vector<Complex> &values)
{
	return to_array_from_complex<float>(values);
}

template <>
std::shared_ptr<arrow::Array> to_array<DComplex>(const std::vector<DComplex> &values)
{
	return to_array_from_complex<double>(values);
}

template <typename T>
std::shared_ptr<arrow::Table> to_table(const std::vector<T> &values)
{
	// Vector to Table with single column
	std::vector<std::shared_ptr<arrow::Field>> fields = {
		arrow::field("DATA", get_table_type<T>())
	};
	auto schema = std::make_shared<arrow::Schema>(fields);
	return arrow::Table::Make(schema, {to_array(values)});
}

::plasma::ObjectID write_to_plasma(const arrow::Table &table)
{
	auto client = get_plasma_client();
	auto object_id = get_random_object_id();

	// Calculate buffer size
	auto sink = *arrow::io::BufferOutputStream::Create();
	auto status = (*arrow::ipc::NewStreamWriter(sink.get(), table.schema()))->WriteTable(table);
	assert(status.ok());
	auto size = *sink->Tell();

	std::shared_ptr<Buffer> buffer;
	if (!client.Create(object_id, size, nullptr, 0, &buffer).ok())
	{
		throw std::runtime_error("create error");
	}
	arrow::io::FixedSizeBufferWriter stream(buffer);
	int32_t meta_len = 0;
	status = (*arrow::ipc::NewStreamWriter(&stream, table.schema()))->WriteTable(table);
	if (!status.ok())
	{
		throw std::runtime_error("write table");
	}
	if (!client.Seal(object_id).ok())
	{
		throw std::runtime_error("seal error");
	}

	//std::this_thread::sleep_for(std::chrono::seconds(1000));
	return object_id;
}

template <typename T, typename CheckingRoutine, typename ColumnT=T>
static void read_and_check(T expected_datum,
  const casacore::IPosition& column_cell_shape, CheckingRoutine && check_routine,
  const casacore::IPosition& tensor_cell_shape = {})
{
	constexpr std::size_t nrows = 256;
	std::vector<T> values;
	std::size_t row;
	std::tie(values, row) = prepare_data(nrows, expected_datum, column_cell_shape);

	auto table = to_table(values);
	auto object_id = write_to_plasma(*table);

	// Create a storage manager and column like a user would using a Table.
	// This is enough to pass down all the information we need
	PlasmaStMan stman("", {}, {{"DATA", object_id.binary()}});
	stman.create64(nrows);
	auto column = stman.makeScalarColumn("DATA", casacore_type_v<ColumnT>, "dataTypeID");

	check_routine(column, nrows, values, row);
}

template <typename T>
static void read_cell()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, assert_scalar_cell<T>);
}

template <typename T>
static void read_column()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, assert_scalar_column<T>);
}

template <typename T>
static void read_column_cells_continuous()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, assert_scalar_continuous_column_cells<T>);
}

template <typename T>
static void read_column_cells_disjoint()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, assert_scalar_disjoint_column_cells<T>);
}

TEST(PlasmaStManArrayReadTests, ReadSingleCell)
{
	FOR_ALL_TYPES(read_cell);
}

TEST(PlasmaStManArrayReadTests, ReadColumn)
{
	FOR_ALL_TYPES(read_column);
}

TEST(PlasmaStManArrayReadTests, ReadColumnCellsContinuous)
{
	FOR_ALL_TYPES(read_column_cells_continuous);
}

TEST(PlasmaStManArrayReadTests, ReadColumnCellsDisjoint)
{
	FOR_ALL_TYPES(read_column_cells_disjoint);
}

} // namespace plasma
} // namespace ska

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}