#include <algorithm>
#include <complex>
#include <utility>
#include <vector>
#include <type_traits>

#include <gtest/gtest.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/tables/Tables/RefRows.h>

#include "ska/plasma/exceptions.h"
#include "ska/plasma/plasma_stman.h"
#include "test_common.h"

namespace ska {
namespace plasma {

template <typename T>
static std::vector<std::int64_t> make_tensor_shape(
  const std::size_t nrows, const casacore::IPosition& cell_shape)
{
	std::vector<std::int64_t> shape {std::int64_t(nrows)};
	shape.insert(shape.end(), cell_shape.begin(), cell_shape.end());
	if (casacore_type_v<T> == casacore::DataType::TpComplex ||
	    casacore_type_v<T> == casacore::DataType::TpDComplex)
	{
		shape.push_back(2);
	}
	return shape;
}


template <typename T, typename CheckingRoutine, typename ColumnT=T>
static void read_and_check(T expected_datum,
  const casacore::IPosition& column_cell_shape, CheckingRoutine && check_routine,
  const casacore::IPosition& tensor_cell_shape = {})
{
	constexpr std::size_t nrows = 256;
	std::vector<T> values;
	std::size_t row;
	std::tie(values, row) = prepare_data(nrows, expected_datum, column_cell_shape);

	auto tensor_shape = [&]() {
		if (!tensor_cell_shape.empty())
		{
			// tensor_cell_shape already takes into account complex data, force
			// it to *not* add it again
			return make_tensor_shape<casacore::Int>(nrows, tensor_cell_shape);
		}
		return make_tensor_shape<T>(nrows, column_cell_shape);
	}();
	auto tensor = to_tensor(values, tensor_shape);
	auto object_id = write_to_plasma(*tensor);

	// Create a storage manager and column like a user would using a Table.
	// This is enough to pass down all the information we need
	PlasmaStMan stman("", {{"DATA", object_id.binary()}});
	stman.create64(nrows);
	auto column = stman.makeDirArrColumn("DATA", casacore_type_v<ColumnT>, "dataTypeID");
	column->setFixedShapeColumn(column_cell_shape);

	check_routine(column, row, nrows, values);
}

template <typename T>
static void read_cell()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, [&](casacore::DataManagerColumn *column, std::size_t row, casacore::rownr_t nrows, const std::vector<T> &values) {
		// Read with the storage manager column. `row` must have the given value,
		// others are default
		casacore::Array<T> cell_data(casacore::IPosition{1});
		column->getArrayV(row, cell_data);
		EXPECT_TRUE(allEQ(cell_data, casacore::Array<T>{expected_datum}));
		if (row < nrows - 1)
		{
			column->getArrayV(row + 1, cell_data);
			EXPECT_TRUE(allEQ(cell_data, casacore::Array<T>{T{}}));
		}
		if (row > 1)
		{
			column->getArrayV(row - 1, cell_data);
			EXPECT_TRUE(allEQ(cell_data, casacore::Array<T>{T{}}));
		}
	});
}

template <typename T>
static void read_column()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, [](casacore::DataManagerColumn *column, std::size_t row, casacore::rownr_t nrows, const std::vector<T> &values) {
		// The full column must be the same as the values that were written via
		// plasma
		casacore::IPosition column_shape {1, ssize_t(nrows)};
		ASSERT_EQ(column_shape.size(), 2);
		casacore::Array<T> column_data {column_shape};
		column->getArrayColumnV(column_data);
		EXPECT_EQ(column_data.size(), nrows);
		EXPECT_EQ(column_data.tovector(), values);
	});
}

template <typename T>
static void read_column_cells_continuous()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, [](casacore::DataManagerColumn *column, std::size_t row, casacore::rownr_t nrows, const std::vector<T> &values) {

		// A single, continuous  row slice with up to 3 rows
		casacore::rownr_t start = row - (row >= 1);
		casacore::rownr_t end = row + (row < nrows - 1);
		auto slice_size = end - start + 1;
		casacore::IPosition column_slice_shape {1, ssize_t(slice_size)};

		casacore::Array<T> column_data {column_slice_shape};
		column->getArrayColumnCellsV(casacore::RefRows(start, end), column_data);

		std::vector<T> expected_slice (slice_size);
		std::copy(values.begin() + start, values.begin() + start + slice_size, expected_slice.begin());
		EXPECT_EQ(column_data.size(), slice_size);
		EXPECT_EQ(column_data.tovector(), expected_slice);
	});
}

template <typename T>
static void read_column_cells_disjoint()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, {1}, [](casacore::DataManagerColumn *column, std::size_t row, casacore::rownr_t nrows, const std::vector<T> &values) {
		// Three disjoint rows, should use default row-by-row behavior
		casacore::rownr_t first = row >= 2 ? row - 2 : 2;
		casacore::rownr_t second = row;
		casacore::rownr_t third = row < nrows - 2 ? row + 2 : nrows - 2;

		casacore::Vector<casacore::rownr_t> rows {first, second, third};
		casacore::Array<T> column_data(casacore::IPosition{1, 3});
		column->getArrayColumnCellsV(casacore::RefRows{rows}, column_data);

		std::vector<T> expected_slice(3);
		expected_slice[0] = values[first];
		expected_slice[1] = values[second];
		expected_slice[2] = values[third];
		EXPECT_EQ(column_data.size(), 3);
		EXPECT_EQ(column_data.tovector(), expected_slice);
	});
}

template <typename T>
static void null_check(casacore::DataManagerColumn *, std::size_t, casacore::rownr_t, const std::vector<T> &)
{
}

template <typename ColumnT, typename T>
static void read_with_incorrect_type(T expected_datum)
{
	static_assert(!std::is_same<T, ColumnT>::value);
	EXPECT_THROW(
		(read_and_check<T, decltype(null_check<T>), ColumnT>(expected_datum, {1}, null_check<T>)),
		exception);
}

template <typename T>
static void read_with_incorrect_shape(
  const casacore::IPosition &column_cell_shape,
  const casacore::IPosition &tensor_cell_shape)
{
	read_and_check(T{}, column_cell_shape, [&](casacore::DataManagerColumn *column, std::size_t row, casacore::rownr_t nrows, const std::vector<T> &values)
	{
		casacore::Array<T> cell_data(column_cell_shape);
		EXPECT_THROW(column->getArrayV(row, cell_data), exception);
	}, tensor_cell_shape);
}

static void read_non_complex_with_incorrect_shape(
  const casacore::IPosition &shape,
  const casacore::IPosition &incorrect_shape)
{
	read_with_incorrect_shape<casacore::uChar>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::Short>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::uShort>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::Int>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::uInt>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::Int64>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::Float>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::Double>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::uChar>(shape, incorrect_shape);
}

static void read_complex_with_incorrect_shape(
  const casacore::IPosition &shape,
  const casacore::IPosition &incorrect_shape)
{
	read_with_incorrect_shape<casacore::Complex>(shape, incorrect_shape);
	read_with_incorrect_shape<casacore::DComplex>(shape, incorrect_shape);
}

TEST(PlasmaStManArrayReadTests, ReadSingleCell)
{
	FOR_ALL_TYPES(read_cell);
}

TEST(PlasmaStManArrayReadTests, IncorrectType)
{
	read_with_incorrect_type<casacore::Int64>(casacore::uChar(123));
	read_with_incorrect_type<casacore::Int64>(casacore::Short(123));
	read_with_incorrect_type<casacore::Int64>(casacore::uShort(123));
	read_with_incorrect_type<casacore::Int64>(casacore::Int(123));
	read_with_incorrect_type<casacore::Int64>(casacore::uInt(123));
	read_with_incorrect_type<casacore::Float>(casacore::Int64(123));
	read_with_incorrect_type<casacore::Int64>(casacore::Float(123));
	read_with_incorrect_type<casacore::Int64>(casacore::Double(123));
	read_with_incorrect_type<casacore::Int64>(casacore::Complex(123.F, 345.F));
	read_with_incorrect_type<casacore::Double>(casacore::Complex(123.F, 345.F));
	read_with_incorrect_type<casacore::Int64>(casacore::DComplex(123., 345.));
	read_with_incorrect_type<casacore::Float>(casacore::DComplex(123., 345.));
}

TEST(PlasmaStManArrayReadTests, IncorrectShape)
{
	// The first dimension must be > 1 because we do -= 1 in some of the tests
	std::vector<casacore::IPosition> shapes = {
		{2}, {45}, {4}, {5}, {10},
		{3, 2}, {2, 1}, {3, 2},
	};

	// main tests
	{
		// incorrect size
		for (auto &shape : shapes) {
			auto incorrect_shape = shape;
			incorrect_shape[0] -= 1;
			ASSERT_NE(incorrect_shape[0], shape[0]);
			read_non_complex_with_incorrect_shape(shape, incorrect_shape);
		}

		// incorrect dimensionality
		for (auto &shape : shapes) {
			auto incorrect_shape = shape;
			incorrect_shape.append({1, 1, 1});
			ASSERT_EQ(incorrect_shape.size(), shape.size() + 3);
			read_non_complex_with_incorrect_shape(shape, incorrect_shape);
		}
	}

	// Same, but for complex types, which require a tensor with an extra dim == 2
	{
		// incorrect size in first dimension
		for (auto &shape : shapes) {
			auto incorrect_shape = shape;
			incorrect_shape.append({2});
			incorrect_shape[0] -= 1;
			ASSERT_NE(incorrect_shape[0], shape[0]);
			read_complex_with_incorrect_shape(shape, incorrect_shape);
		}

		// incorrect size in last additional dimension
		for (auto &shape : shapes) {
			auto incorrect_shape = shape;
			incorrect_shape.append({1});
			ASSERT_NE(incorrect_shape[incorrect_shape.size() - 1], 2);
			read_complex_with_incorrect_shape(shape, incorrect_shape);
		}

		// incorrect dimensionality
		for (auto &shape : shapes) {
			auto incorrect_shape = shape;
			incorrect_shape.append({2, 1, 1, 1});
			ASSERT_EQ(incorrect_shape.size(), shape.size() + 4);
			read_complex_with_incorrect_shape(shape, incorrect_shape);
		}

		// incorrect dimensionality (but still ends with 2)
		for (auto &shape : shapes) {
			auto incorrect_shape = shape;
			incorrect_shape.append({1, 1, 1, 2});
			ASSERT_EQ(incorrect_shape.size(), shape.size() + 4);
			read_complex_with_incorrect_shape(shape, incorrect_shape);
		}
	}
}

TEST(PlasmaStManArrayReadTests, ReadColumn)
{
	FOR_ALL_TYPES(read_column);
}

TEST(PlasmaStManArrayReadTests, ReadColumnCellsContinuous)
{
	FOR_ALL_TYPES(read_column_cells_continuous);
}

TEST(PlasmaStManArrayReadTests, ReadColumnCellsDisjoint)
{
	FOR_ALL_TYPES(read_column_cells_disjoint);
}


} // namespace plasma
} // namespace ska

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}