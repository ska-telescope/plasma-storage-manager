#include <chrono>

#include <casacore/casa/Containers/Record.h>
#include <gtest/gtest.h>

#include "ska/plasma/exceptions.h"
#include "ska/plasma/plasma_stman.h"
#include "test_common.h"

namespace ska
{
namespace plasma
{

TEST(PlasmaStManBasicTests, Create)
{
	PlasmaStMan stman;
}

TEST(PlasmaStManBasicTests, CreateWithTensorMapping)
{
	PlasmaStMan stman {"", {{"column_name", VALID_OBJECT_ID1}}};
}

TEST(PlasmaStManBasicTests, CreateWithBothMappings)
{
	PlasmaStMan stman {"", {{"column_name", VALID_OBJECT_ID1}}, {{"column_name2", VALID_OBJECT_ID2}}};
}

TEST(PlasmaStManBasicTests, CreateWithInvalidObjectIDs)
{
	for (const auto &invalid: {INVALID_OBJECT_ID1, INVALID_OBJECT_ID2, INVALID_OBJECT_ID3}) {
		EXPECT_THROW(
			(PlasmaStMan{"", {{"column_name", invalid}}}),
			invalid_object_id
		);
		EXPECT_THROW(
			(PlasmaStMan{"", {{"column_name", VALID_OBJECT_ID1}}, {{"column_name2", invalid}}}),
			invalid_object_id
		);
	}
}

TEST(PlasmaStManBasicTests, Clone)
{
	PlasmaStMan stman {"", {{"column_name", VALID_OBJECT_ID1}}};
	auto the_clone = stman.clone();
	delete the_clone;
}

TEST(PlasmaStManBasicTests, MakeObject)
{
	PlasmaStMan::makeObject("name", casacore::Record{});
}

TEST(PlasmaStManBasicTests, MakeObjectString)
{
	casacore::Record spec;
	casacore::Record data;
	data.define("DATA", "00000000000000000000");
	spec.defineRecord("TENSOROBJECTIDS", data);
	PlasmaStMan::makeObject("name", spec);
}

TEST(PlasmaStManBasicTests, MakeObjectInt64Array)
{
	casacore::Record spec;
	casacore::Record data;
	casacore::Array<casacore::Int64> objectId = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	data.define("DATA", objectId);
	spec.defineRecord("TENSOROBJECTIDS", data);
	PlasmaStMan::makeObject("name", spec);
}

TEST(PlasmaStManBasicTests, MakeObjectInt64CArray)
{
	casacore::Record spec;
	casacore::Record data;
	int64_t objectId[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	data.define("DATA", objectId);
	spec.defineRecord("TENSOROBJECTIDS", data);
	EXPECT_THROW(PlasmaStMan::makeObject("name", spec), ska::plasma::type_error);
}

TEST(PlasmaStManBasicTests, MakeObjectInt8Array)
{
	casacore::Record spec;
	casacore::Record data;
	casacore::Array<casacore::uChar> objectId = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	data.define("DATA", objectId);
	spec.defineRecord("TENSOROBJECTIDS", data);
	EXPECT_THROW(PlasmaStMan::makeObject("name", spec), ska::plasma::type_error);
}

TEST(PlasmaStManBasicTests, MakeObjectStringArray)
{
	casacore::Record spec;
	casacore::Record data;
	casacore::Array<casacore::String> objectIds = {"00000000000000000000", "00000000000000000001"};
	data.define("DATA", objectIds);
	spec.defineRecord("TENSOROBJECTIDS", data);
	EXPECT_THROW(PlasmaStMan::makeObject("name", spec), ska::plasma::type_error);
}

TEST(PlasmaStManBasicTests, PingPlasma)
{
	PlasmaStMan stman;
	stman.ping_plasma();
}

static void assert_fails_quickly_to_connect(PlasmaStMan &stman)
{
	ASSERT_EQ(stman.getProperties().asInt("PLASMACONNECTRETRIES"), 0);
	auto start = std::chrono::system_clock::now();
	EXPECT_THROW(stman.ping_plasma(), plasma_error);
	auto end = std::chrono::system_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
	EXPECT_LT(duration, 1000);
}

TEST(PlasmaStManBasicTests, FailsQuicklyToConnect)
{
	PlasmaStMan stman {"this-socket-does-not-exist"};
	stman.set_plasma_connect_retries(0);
	assert_fails_quickly_to_connect(stman);
}

TEST(PlasmaStManBasicTests, FailsQuicklyToConnectViaProperties)
{
	PlasmaStMan stman {"this-socket-does-not-exist"};
	casacore::Record properties;
	properties.define("PLASMACONNECTRETRIES", 0);
	stman.setProperties(properties);
	assert_fails_quickly_to_connect(stman);
}

static void assert_fails_quickly_to_get_from_plasma(PlasmaStMan &stman)
{
	ASSERT_EQ(stman.getProperties().asInt("PLASMAGETTIMEOUT"), 1);
	stman.ping_plasma();
	auto start = std::chrono::system_clock::now();
	EXPECT_THROW(
		stman.makeDirArrColumn("DATA", casacore::DataType::TpInt, "dataTypeID"),
		plasma_error
	);
	auto end = std::chrono::system_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
	EXPECT_LT(duration, 100);
}

TEST(PlasmaStManBasicTests, FailsQuicklyToGetFromPlasma)
{
	// Set get_timeout = 1 millisecond (instead of the default 10000), reading
	// from plasma should fail well within a second
	PlasmaStMan stman{"", {{"DATA", VALID_OBJECT_ID1}}};
	stman.set_plasma_get_timeout(1);
	assert_fails_quickly_to_get_from_plasma(stman);
}

TEST(PlasmaStManBasicTests, FailsQuicklyToGetFromPlasmaViaProperties)
{
	// Like above, but using properties
	PlasmaStMan stman{"", {{"DATA", VALID_OBJECT_ID1}}};
	casacore::Record properties;
	properties.define("PLASMAGETTIMEOUT", 1);
	stman.setProperties(properties);
	assert_fails_quickly_to_get_from_plasma(stman);
}

} // namespace plasma
} // namespace ska

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}