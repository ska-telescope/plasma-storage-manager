#ifndef SRC_SKA_PLASMA_TABLE_READER_H
#define SRC_SKA_PLASMA_TABLE_READER_H

#include <memory>

#include <arrow/io/interfaces.h>
#include <arrow/table.h>

#include "ska/plasma/arrow_reader.h"

namespace ska {
namespace plasma {

/**
 * An ArrowReader that reads data off an Arrow Table.
 *
 * Tables can contain multiple "fields" or "columns". The column read by this
 * reader is the one with the same name of the casacore Table column backed up
 * by this reader. If no such field/column is found in the Arrow Table then an
 * error is raised. Only Tables written as a single BatchRecord are currently
 * supported.
 */
class TableReader : public ArrowReader
{
private:
	/// The Arrow Table hosting the column this reader reads data from
	std::shared_ptr<arrow::Table> _table;

	/**
	 * @return The column in the Arrow Table this reader reads data from, as a
	 * ChunkArray pointer.
	 */
	const std::shared_ptr<arrow::ChunkedArray> column() const;

	/// @see read_scalar
	template <typename CasacoreType>
	void read_scalar_impl(rownr_t rownr, void *dataPtr);

	/// @see read_array
	template <typename CasacoreType>
	void read_array_impl(ArrayBase &array, std::size_t offset);

	/// @see read_scalar
	template <typename ComplexT>
	void read_complex_scalar(rownr_t rownr, void *dataPtr);

	/// @see read_array
	template <typename ComplexT>
	void read_complex_array(ArrayBase &array, std::size_t offset);

protected:
	void check_shape(const Shape &column_shape) const override;
	void check_types() const override;

public:
	/**
	 * Constructs a TableReader for the given casacore data type and column
	 * from an input stream. The column name in casacore must be the same as
	 * the column in the Arrow Table that will be read.
	 * @param column_name The casacore column backed by this reader. Should be
	 * the same as the column in the Arrow Table.
	 * @param data_type The casacore data type of the column backed by this
	 * @param input_stream The input stream from where the Table will be read.
	 * This is possibly created from an object read from Plasma.
	 */
	TableReader(const std::string &column_name, casacore::DataType data_type,
	  arrow::io::InputStream* input_stream);

	void read_scalar(rownr_t rownr, void *dataPtr) override;
	void read_array(ArrayBase &array, std::size_t offset) override;

};

}  // namespace plasma
}  // namespace ska

#endif /* SRC_SKA_PLASMA_TABLE_READER_H */