#include <cassert>
#include <cstdlib>
#include <stdexcept>

#include <casacore/casa/Containers/Record.h>
#include <casacore/casa/IO/AipsIO.h>

#include "ska/plasma/casacore_types.h"
#include "ska/plasma/exceptions.h"
#include "ska/plasma/plasma_stman_column.h"
#include "ska/plasma/plasma_stman_impl.h"


namespace ska {

namespace plasma {

constexpr const char *PlasmaStMan::impl::DATAMAN_NAME;
constexpr const char *PlasmaStMan::impl::SPEC_FIELD_PLASMA_SOCKET;
constexpr const char *PlasmaStMan::impl::SPEC_FIELD_TENSOR_OBJECT_IDS;
constexpr const char *PlasmaStMan::impl::SPEC_FIELD_TABLE_OBJECT_IDS;
constexpr const char *PlasmaStMan::impl::PROPERTY_PLASMA_CONNECT_RETRIES;
constexpr const char *PlasmaStMan::impl::PROPERTY_PLASMA_GET_TIMEOUT;

PlasmaStMan::impl::impl(std::string plasma_socket,
  std::map<std::string, ObjectID> tensor_object_ids,
  std::map<std::string, ObjectID> table_object_ids)
  : _client(std::move(plasma_socket)),
    _tensor_object_ids(std::move(tensor_object_ids)),
    _table_object_ids(std::move(table_object_ids))
{
}

static bool contains(const std::map<std::string, ObjectID> &map,
  const std::string &key)
{
	return map.find(key) != map.end();
}

ArrowObjectInfo PlasmaStMan::impl::arrow_info(const std::string &column_name) const
{
	if (contains(_tensor_object_ids, column_name))
	{
		return {ArrowObjectType::TENSOR, _tensor_object_ids.at(column_name)};
	}
	else if (contains(_table_object_ids, column_name))
	{
		return {ArrowObjectType::TABLE, _table_object_ids.at(column_name)};
	}
	throw no_column_mapping_error(column_name);
}

PlasmaStMan::impl::~impl() = default;

void PlasmaStMan::impl::ping_plasma()
{
	_client.ping();
}

void PlasmaStMan::impl::set_plasma_get_timeout(std::int64_t timeout)
{
	_client.set_get_timeout(timeout);
}

void PlasmaStMan::impl::set_plasma_connect_retries(int connect_retries)
{
	_client.set_connect_retries(connect_retries);
}

DataManager *PlasmaStMan::impl::clone() const
{
	assert(_columns.empty());
	return new PlasmaStMan(_client.socket(), _tensor_object_ids, _table_object_ids);
}

String PlasmaStMan::impl::dataManagerType() const
{
	return DATAMAN_NAME;
}

String PlasmaStMan::impl::dataManagerName() const
{
	return DATAMAN_NAME;
}

void PlasmaStMan::impl::create64(rownr_t aNrRows)
{
	_nrows = aNrRows;
}

rownr_t PlasmaStMan::impl::open64(rownr_t aRowNr, AipsIO &ios)
{
	_nrows = aRowNr;
	read_metadata(ios);
	// All columns should now point to an existing tensor
	for (auto &column : _columns)
	{
		if (!column->reader_initialized())
		{
			column->initialize_reader(arrow_info(column->columnName()));
		}
	}
	return aRowNr;
}

void read_mapping(AipsIO &ios, std::map<std::string, ObjectID> &object_ids)
{
	casacore::uInt64 n_cols = 0;
	ios >> n_cols;
	for (std::size_t i = 0; i != n_cols; i++)
	{
		String column_name;
		String object_id;
		ios >> column_name;
		ios >> object_id;
		object_ids[column_name] = object_id;
	}
}

void PlasmaStMan::impl::read_metadata(AipsIO &ios)
{
	ios.getstart(DATAMAN_NAME);
	read_mapping(ios, _tensor_object_ids);
	read_mapping(ios, _table_object_ids);
	ios.getend();
}

void write_mapping(AipsIO &ios, const std::map<std::string, ObjectID> &object_ids)
{
	ios << casacore::uInt64(object_ids.size());
	for (auto &item : object_ids)
	{
		ios << item.first;
		ios << item.second.string();
	}
}

void PlasmaStMan::impl::write_metadata(AipsIO &ios)
{
	ios.putstart(DATAMAN_NAME, 1);
	assert(_columns.size() == _tensor_object_ids.size() + _table_object_ids.size());
	write_mapping(ios, _tensor_object_ids);
	write_mapping(ios, _table_object_ids);
	ios.putend();
}

rownr_t PlasmaStMan::impl::resync64(rownr_t /*aRowNr*/)
{
	throw UNIMPLEMENTED();
}

Bool PlasmaStMan::impl::flush(AipsIO &ios, Bool /*doFsync*/)
{
	write_metadata(ios);
	return true;
}

DataManagerColumn *PlasmaStMan::impl::makeScalarColumn(
    const String &aName, int aDataType,
    const String &aDataTypeID)
{
	return makeColumnCommon(aName, aDataType, aDataTypeID);
}

DataManagerColumn *PlasmaStMan::impl::makeDirArrColumn(
    const String &aName, int aDataType,
    const String &aDataTypeID)
{
	return makeColumnCommon(aName, aDataType, aDataTypeID);
}

DataManagerColumn *PlasmaStMan::impl::makeIndArrColumn(
    const String &aName, int aDataType,
    const String &aDataTypeID)
{
	return makeColumnCommon(aName, aDataType, aDataTypeID);
}

DataManagerColumn *PlasmaStMan::impl::makeColumnCommon(
    const String &aName, int aDataType,
    const String &/*aDataTypeID*/)
{
	ArrowObjectInfo object_info;
	try {
		object_info = arrow_info(aName);
	} catch (const no_column_mapping_error &) {
		// column needs to be given a proper object id later on at open64()
	}
	_columns.emplace_back(
	    std::make_unique<PlasmaStManColumn>(
	        aName, _client, *this, object_info, aDataType));
	return _columns.back().get();
}


void PlasmaStMan::impl::deleteManager()
{
}

void PlasmaStMan::impl::addRow64(rownr_t /*aNrRows*/)
{
}

static void from_map(Record &spec, const std::string &field_name,
  const std::map<std::string, ObjectID> &map)
{
	Record record;
	for (const auto &item: map)
	{
		record.define(item.first, item.second.string());
	}
	spec.defineRecord(field_name, record);
}

Record PlasmaStMan::impl::dataManagerSpec() const
{
	Record rec;
	rec.define(SPEC_FIELD_PLASMA_SOCKET, _client.socket());
	from_map(rec, SPEC_FIELD_TENSOR_OBJECT_IDS, _tensor_object_ids);
	from_map(rec, SPEC_FIELD_TABLE_OBJECT_IDS, _table_object_ids);
	rec.merge(getProperties());
	return rec;
}

Record PlasmaStMan::impl::getProperties() const
{
	Record rec;
	rec.define(PROPERTY_PLASMA_CONNECT_RETRIES, _client.connect_retries());
	rec.define(PROPERTY_PLASMA_GET_TIMEOUT, static_cast<Int64>(_client.get_timeout()));
	return rec;
}

void PlasmaStMan::impl::setProperties(const Record &props)
{
	if (props.isDefined(PROPERTY_PLASMA_CONNECT_RETRIES))
	{
		set_plasma_connect_retries(props.asInt(PROPERTY_PLASMA_CONNECT_RETRIES));
	}
	if (props.isDefined(PROPERTY_PLASMA_GET_TIMEOUT))
	{
		set_plasma_get_timeout(props.asInt64(PROPERTY_PLASMA_GET_TIMEOUT));
	}
}

casacore::String to_object_id(const casacore::Array<Int64>& object_id_array)
{
	constexpr casacore::String::size_type OBJECT_ID_SIZE = 20;
	assert(object_id_array.size() == OBJECT_ID_SIZE);
	auto object_id = casacore::String(OBJECT_ID_SIZE, '0');
	for(casacore::String::size_type i = 0; i < OBJECT_ID_SIZE; i++)
	{
		object_id[i] = static_cast<casacore::Char>(object_id_array(IPosition(1, static_cast<Int64>(i))));
	}
	return object_id;
}

auto to_map(const Record &spec, const std::string &field_name)
{
	std::map<std::string, ObjectID> map;
	if (spec.isDefined(field_name))
	{
		const auto &from_record = spec.asRecord(field_name);
		for (Int i = 0; i != Int(from_record.size()); i++)
		{
			auto column_name = from_record.name(i);
			casacore::String object_id;
			if(from_record.type(i) == casacore::DataType::TpString)
			{
				object_id = from_record.asString(i);
			}
			else if(from_record.type(i) == casacore::DataType::TpArrayInt64)
			{
				object_id = to_object_id(from_record.asArrayInt64(i));
			}
			else
			{
				std::stringstream ss;
				ss << "Invalid plasma ObjectId type: " << from_record.type(i);
				throw type_error(ss.str());
			}
			map[column_name] = object_id;
		}
	}
	return map;
}

DataManager *PlasmaStMan::impl::makeObject(
    const String &/*aDataManType*/, const Record &spec)
{
	std::string plasma_socket;
	if (spec.isDefined(SPEC_FIELD_PLASMA_SOCKET))
	{
		plasma_socket = spec.asString(SPEC_FIELD_PLASMA_SOCKET);
	}
	auto tensor_object_ids = to_map(spec, SPEC_FIELD_TENSOR_OBJECT_IDS);
	auto table_object_ids = to_map(spec, SPEC_FIELD_TABLE_OBJECT_IDS);
	auto *stman = new PlasmaStMan(plasma_socket, tensor_object_ids, table_object_ids);
	stman->setProperties(spec);
	return stman;
}

} // namespace plasma
} // namespace ska
