#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>

#include <arrow/io/memory.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/IO/ArrayIO.h>
#include <casacore/tables/Tables/RefRows.h>

#include "ska/plasma/casacore_types.h"
#include "ska/plasma/exceptions.h"
#include "ska/plasma/plasma_stman_column.h"
#include "ska/plasma/plasma_stman_impl.h"
#include "ska/plasma/table_reader.h"
#include "ska/plasma/tensor_reader.h"
#include "ska/plasma/types.h"

namespace ska {
namespace plasma {

static bool is_single_continuous_slice(const RefRows &rownrs)
{
	auto &row_vector = rownrs.rowVector();
	return rownrs.isSliced() && row_vector.size() == 3 && row_vector[2] == 1;
}

#ifndef NDEBUG
static void assert_array_fits_slice(const ArrayBase &array,
  const RefRows &rownrs)
{
	const auto &row_vector = rownrs.rowVector();
	auto start = row_vector[0];
	auto end = row_vector[1];
	assert(end >= start);
	assert(ssize_t(end - start + 1) == array.shape()[array.shape().size() - 1]);
}
#else
static void assert_array_fits_slice(const ArrayBase &/*array*/,
  const RefRows &/*rownrs*/)
{
}
#endif // NDEBUG

PlasmaStManColumn::PlasmaStManColumn(const std::string &name,
    PlasmaClient &client,
    PlasmaStMan::impl &storage_manager,
    const ArrowObjectInfo &object_info, int dataType)
  : casacore::StManColumnBase(dataType), _client(client),
    _storage_manager(storage_manager)
{
	setColumnName(name);
	initialize_reader(object_info);
}

void PlasmaStManColumn::initialize_reader(const ArrowObjectInfo &object_info)
{
	if (!object_info.object_id.valid())
	{
		return;
	}

	assert(object_info.type == ArrowObjectType::TENSOR ||
	       object_info.type == ArrowObjectType::TABLE);

	auto object_buffer = _client.get(object_info.object_id);
	::arrow::io::BufferReader buffer_reader(object_buffer.data);

	if (object_info.type == ArrowObjectType::TABLE) {
		_reader = std::make_unique<TableReader>(columnName(), dtype(), &buffer_reader);
	}
	else {
		_reader = std::make_unique<TensorReader>(columnName(), dtype(), &buffer_reader);
	}
}

Shape PlasmaStManColumn::get_column_shape() const
{
	// Add nrows to the cell shape to get the column shape
	auto nrows = _storage_manager.nrows();
	assert(nrows < 0x8000000000000000);
	auto column_shape = Shape(_shape.begin(), _shape.end());
	column_shape.push_back(static_cast<std::int64_t>(nrows));
	return column_shape;
}

void PlasmaStManColumn::check_conformance() const
{
	if (_conformance_checked)
	{
		return;
	}
	// The column should have been properly initialised at this point, meaning
	// it should have an associated reader object by now
	assert(_reader);
	_reader->check_conformance(get_column_shape());
	_conformance_checked = true;
}

bool PlasmaStManColumn::reader_initialized() const
{
	return static_cast<bool>(_reader);
}

void PlasmaStManColumn::setShapeColumn(const IPosition &aShape)
{
	_shape = aShape;
}

IPosition PlasmaStManColumn::shape(rownr_t /*rownr*/)
{
	return _shape;
}

void PlasmaStManColumn::read_array(ArrayBase &array, std::size_t offset)
{
	check_conformance();
	_reader->read_array(array, offset);
}

void PlasmaStManColumn::getArrayV(rownr_t rownr, ArrayBase &array)
{
	auto offset = std::size_t(_shape.product());
	offset *= rownr;
	assert(array.shape() == _shape);
	read_array(array, offset);
}

void PlasmaStManColumn::getArrayColumnV(ArrayBase &array)
{
	assert(Shape(array.shape().begin(), array.shape().end()) == get_column_shape());
	read_array(array, 0);
}

void PlasmaStManColumn::getArrayColumnCellsV(const RefRows& rownrs, ArrayBase& array)
{
	// We currently support a a single, continuous slice of rows
	// In the future we could support multiple slices, which would reduce the
	// amount of copy operations
	if (!is_single_continuous_slice(rownrs))
	{
		return casacore::StManColumnBase::getArrayColumnCellsV(rownrs, array);
	}
	assert_array_fits_slice(array, rownrs);
	auto first_row = rownrs.rowVector()[0];
	read_array(array, std::size_t(_shape.product()) * first_row);
}

// NOLINTNEXTLINE
#define GET_PUT_T(NAME, TYPE)                                                      \
void PlasmaStManColumn::get ## NAME(rownr_t aRowNr, TYPE *aDataPtr)                \
{                                                                                  \
    check_conformance();                                                           \
    _reader->read_scalar(aRowNr, aDataPtr);                                        \
}                                                                                  \
                                                                                   \

void PlasmaStManColumn::getScalarColumnV(ArrayBase& array)
{
	assert(Shape(array.shape().begin(), array.shape().end()) == get_column_shape());
	read_array(array, 0);
}

void PlasmaStManColumn::getScalarColumnCellsV(const RefRows &rownrs, ArrayBase& array)
{
	if (!is_single_continuous_slice(rownrs))
	{
		return casacore::StManColumnBase::getScalarColumnCellsV(rownrs, array);
	}
	assert_array_fits_slice(array, rownrs);
	auto first_row = rownrs.rowVector()[0];
	read_array(array, first_row);
}

GET_PUT_T(Bool, Bool)
GET_PUT_T(uChar, uChar)
GET_PUT_T(Short, Short)
GET_PUT_T(uShort, uShort)
GET_PUT_T(Int, Int)
GET_PUT_T(uInt, uInt)
GET_PUT_T(Int64, Int64)
GET_PUT_T(float, Float)
GET_PUT_T(double, Double)
GET_PUT_T(Complex, Complex)
GET_PUT_T(DComplex, DComplex)


} // namespace plasma
} // namespace ska
