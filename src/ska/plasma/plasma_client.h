#ifndef SKA_PLASMA_PLASMA_CLIENT_H
#define SKA_PLASMA_PLASMA_CLIENT_H

#include <plasma/client.h>

#include "ska/plasma/object_id.h"

namespace ska
{
namespace plasma
{

/**
 * A class encapsulating access to a Plasma Store.
 *
 * This class encapsulates access to a Plasma Store. Although it's a very thin
 * wrapper around ::plasma::PlasmaClient, it adds configuration capabilities
 * around certain aspects, like timeouts, the socket to connect to, retries and
 * others.
 */
class PlasmaClient
{
private:
	/// The socket through which the Plasma store is exposed
	std::string _socket;
	/// The underlying Plasma client object.
	::plasma::PlasmaClient _client;
	/// The timeout for the Plasma Get operation, in milliseconds.
	std::int64_t _get_timeout = 10000;
	/// The number of attempts to connect to the Plasma socket before failing.
	int _connect_retries = 50;
	/// Whether we are already connected to Plasma or not.
	bool _connected = false;

	/// Connect the underlying if it's not connected yet.
	void ensure_connected();

public:

	/**
	 * Create a new PlasmaClient that will connect to the given socket.
	 * @param socket The Plasma socket to connect to.
	 */
	PlasmaClient(std::string socket);

	/**
	 * Ensure communication between the client and the server works.
	 */
	void ping();

	/**
	 * Set the timeout for the Plasma Get operation, in milliseconds.
	 * @param timeout The timeout for the Plasma Get operation, in milliseconds.
	 */
	void set_get_timeout(std::int64_t timeout)
	{
		_get_timeout = timeout;
	}

	/**
	 * @return The timeout for the Plasma Get operation, in milliseconds.
	 */
	std::int64_t get_timeout() const
	{
		return _get_timeout;
	}

	/**
	 * Set the number of attempts to connect to the Plasma socket before failing.
	 * @param connect_retries the number of attempts to connect to the Plasma
	 * socket before failing.
	 */
	void set_connect_retries(int connect_retries)
	{
		_connect_retries = connect_retries;
	}

	/**
	 * @return The number of attempts to connect to the Plasma socket before
	 * failing.
	 */
	int connect_retries() const
	{
		return _connect_retries;
	}

	/**
	 * Read an object from the Plasma store. A plasma_error exception is thrown
	 * if no such object is found within the timeout.
	 * @param object_id The ID of the object to read.
	 * @return A Plasma Object Buffer pointing to the object in the Plasma
	 * Store.
	 */
	::plasma::ObjectBuffer get(const ObjectID &object_id);

	/**
	 * @return The socket where this Plasma client connects to.
	 */
	std::string socket() const
	{
		return _socket;
	}
};

}  // namespace plasma
}  // namespace ska

#endif // SKA_PLASMA_PLASMA_CLIENT_H