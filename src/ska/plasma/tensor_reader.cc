#include <arrow/array.h>
#include <arrow/ipc/reader.h>
#include <arrow/tensor.h>

#include "ska/plasma/arrow_buffer_access.h"
#include "ska/plasma/exceptions.h"
#include "ska/plasma/tensor_reader.h"
#include "ska/plasma/types.h"

namespace ska {
namespace plasma {

TensorReader::TensorReader(const std::string &column_name,
  casacore::DataType data_type, ::arrow::io::InputStream *input_stream)
  : ArrowReader(column_name, data_type)
{
	auto tensor = arrow::ipc::ReadTensor(input_stream);
	if (!tensor.ok())
	{
		throw plasma_error("problem reading tensor");
	}
	_tensor = std::move(tensor).MoveValueUnsafe();
	if (!_tensor->is_row_major())
	{
		throw exception("Only row-major tensors are supported");
	}
	check_types();
}

void TensorReader::check_types() const
{
	check_expected(*_tensor->type(), *get_tensor_type(casacore_type()));
}

void TensorReader::check_shape(const Shape &column_shape) const
{
	// Arrow objects don't know about complex types
	using DataType = casacore::DataType;
	auto arrow_shape = _tensor->shape();
	if (casacore_type() == DataType::TpComplex ||
	    casacore_type() == DataType::TpDComplex)
	{
		auto last_dim = arrow_shape.back();
		if (last_dim != 2)
		{
			throw exception("Arrow object does not have final dimension of 2 for complex data type");
		}
		arrow_shape.pop_back();
	}

	// casacore is column-major, while we accept row-major tensors only
	std::reverse(arrow_shape.begin(), arrow_shape.end());

	check_expected(arrow_shape, column_shape);
}

template <typename CasacoreType>
void TensorReader::read_scalar_impl(rownr_t rownr, void *dataPtr)
{
	read_scalar_from_buffer<CasacoreType>(_tensor->data(), rownr, dataPtr);
}

void TensorReader::read_scalar(rownr_t rownr, void *dataPtr)
{
	INVOKE_FOR_DATATYPE(
		casacore_type(),
		read_scalar_impl,
		rownr, dataPtr
	);
}

template <typename CasacoreType>
void TensorReader::read_array_impl(ArrayBase &array, std::size_t offset)
{
	read_array_from_buffer<CasacoreType>(_tensor->data(), offset, array);
}

void TensorReader::read_array(ArrayBase &array, std::size_t offset)
{
	INVOKE_FOR_DATATYPE(
		casacore_type(),
		read_array_impl,
		array, offset
	);
}

}  // namespace plasma
}  // namespace ska