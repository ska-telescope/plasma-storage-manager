#include "ska/plasma/exceptions.h"
#include "ska/plasma/plasma_client.h"

namespace ska
{
namespace plasma
{

PlasmaClient::PlasmaClient(std::string socket)
  : _socket(std::move(socket))
{
	if (_socket.empty())
	{
		const auto *plasma_socket_env = std::getenv("PLASMA_SOCKET");
		if (plasma_socket_env)
		{
			_socket = plasma_socket_env;
		}
		else
		{
			_socket = "/tmp/plasma";
		}
	}
}

void PlasmaClient::ensure_connected()
{
	if (_connected)
	{
		return;
	}
	if (!_client.Connect(_socket, "", 0, _connect_retries).ok())
	{
		throw plasma_error("connection failed");
	}
	_connected = true;
}

::plasma::ObjectBuffer PlasmaClient::get(const ObjectID &object_id)
{
	ensure_connected();
	auto _object_id = ::plasma::ObjectID::from_binary(object_id.string());
	::plasma::ObjectBuffer object_buffer;
	if (!_client.Get(&_object_id, 1, _get_timeout, &object_buffer).ok() ||
	    (!object_buffer.data && !object_buffer.metadata))
	{
		throw plasma_error("cannot read object from plasma");
	}
	return object_buffer;
}

void PlasmaClient::ping()
{
	ensure_connected();
	// we don't really care about the result of DebugString, we just want to
	// contact the server
	_client.DebugString();
}

}  // namespace plasma
}  // namespace ska