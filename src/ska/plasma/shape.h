#ifndef SRC_SKA_PLASMA_SHAPE_H
#define SRC_SKA_PLASMA_SHAPE_H

#include <vector>
#include <cstdint>

namespace ska {
namespace plasma {

/// Common type for representing Arrow and Casacore shapes
using Shape = std::vector<std::int64_t>;

}  // namespace plasma
}  // namespace ska

#endif /* SRC_SKA_PLASMA_SHAPE_H */