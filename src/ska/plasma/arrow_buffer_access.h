#ifndef SRC_SKA_PLASMA_ARROW_BUFFER_ACCESS_H
#define SRC_SKA_PLASMA_ARROW_BUFFER_ACCESS_H

#include <arrow/buffer.h>
#include <casacore/casa/Arrays/Array.h>

#include "ska/plasma/casacore_types.h"

namespace ska {
namespace plasma {

/**
 * Reads a single scalar value from the given Arrow buffer at the given offset.
 * @tparam CasacoreType The (casacore) data type of the values in the buffer.
 * @param arrow_buffer The Arrow Buffer object to read the scalar from.
 * @param offset The offset at which the scalar should be read.
 * @param user_data The address where the scalar should be read into.
 */
template <typename CasacoreType>
void read_scalar_from_buffer(std::shared_ptr<arrow::Buffer> &&arrow_buffer,
  std::size_t offset, void *user_data)
{
	const auto *array_buffer = reinterpret_cast<const CasacoreType *>(arrow_buffer->data());
	*static_cast<CasacoreType *>(user_data) = *(array_buffer + offset);
}

/**
 * Read an array from the Arrow Buffer starting at the given offset. The array's
 * shape determines how much data is effectively read, and might or might not be
 * able to be created with zero-copy.
 *
 * @tparam CasacoreType The (casacore) data type of the values in the buffer.
 * @param arrow_buffer The Arrow Buffer object from where the array should be
 * read.
 * @param offset The offset in the Arrow Buffer at which reading will start.
 * @param array The array where the data should be read into.
 */
template <typename CasacoreType>
void read_array_from_buffer(std::shared_ptr<arrow::Buffer> &&arrow_buffer,
  std::size_t offset, ArrayBase &array)
{
	using typed_array = casacore::Array<CasacoreType>;

	// Creation of casacore Arrays needs a non-const buffer, even when
	// the storage policy is SHARE
	const auto *raw_buffer = reinterpret_cast<const CasacoreType *>(arrow_buffer->data());
	auto *non_const_plasma_buffer = const_cast<CasacoreType *>(raw_buffer);
	auto new_array = typed_array(
		array.shape(), non_const_plasma_buffer + offset,
		casacore::StorageInitPolicy::SHARE
	);

	// If the given array is already a slice of another array then we cannot
	// simply reference our tensor-wrapping array, but we rather need a copy
	auto array_with_type = static_cast<typed_array *>(&array);
	if (array_with_type->nrefs() == 1)
	{
		array_with_type->reference(new_array);
	}
	else
	{
		*array_with_type = new_array;
	}
}

}  // namespace plasma
}  // namespace ska

#endif /* SRC_SKA_PLASMA_ARROW_BUFFER_ACCESS_H */