#ifndef SKA_PLASMA_PLASMA_STMAN_COLUMN_H
#define SKA_PLASMA_PLASMA_STMAN_COLUMN_H

#include <map>
#include <string>

#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/Utilities/DataType.h>
#include <casacore/tables/DataMan/StManColumnBase.h>

#include "ska/plasma/arrow_reader.h"
#include "ska/plasma/casacore_types.h"
#include "ska/plasma/object_id.h"
#include "ska/plasma/plasma_client.h"
#include "ska/plasma/plasma_stman_impl.h"
#include "ska/plasma/shape.h"

namespace ska {
namespace plasma {

/**
 * An enumeration of the different Arrow objects supported by PlasmaStManColumn.
 */
enum class ArrowObjectType
{
	UNKNOWN = 0,  ///< Type still unknown
	TENSOR,       ///< An Apache Arrow Tensor
	TABLE,        ///< An Apache Arrow Table
};

/// A structure holding an Arrow object's type and its ID on a Plasma store
struct ArrowObjectInfo
{
	/// The type of the Arrow object
	ArrowObjectType type = ArrowObjectType::UNKNOWN;
	/// The Object ID of the object in Plasma
	ObjectID object_id;
};

/**
 * A single column of the Plasma Storage Manager
 *
 * A PlasmaStManColumn manages a single column on a casacore Table, which will
 * be backed up by an Arrow object stored in Plasma. The actual handling of the
 * underlying Arrow object is done via an ArrowReader instace, which hides the
 * differences between the different types of Arrow objects that can hold data.
 * At the moment the only supported reader is TensorReader (and thus this class
 * still silently assumes that), but more will come. When the Tensor is
 * retrieved from Plasma this class will create the corresponding TensorReader
 * instance, which will ensure the data types are compatible. Also, upon data
 * access (again, through the reader), the tensor's shape is compared against
 * the column's cell shape to ensure the tensor and the column define the same
 * dimensionality.
 *
 * While casacore is column-major, Arrow is by default row-major. On the other
 * hand, the dimensions that this column receives via setShapeColumn are those
 * of individual cells, while Arrow Tensors will contain the full column data.
 * Thus:
 *
 *  * The first dimension of the Tensor should always be the number of rows of
 *    the column
 *  * For the rest of the dimensions, they should match the column cell's shape
 *    in reverse order.
 *
 * In principle support for non-row-major Tensors should be possible to add, but
 * that is left as a future improvement.
 */
class PlasmaStManColumn : public casacore::StManColumnBase
{

private:
	/// A reference to the client used to connect to plasma
	PlasmaClient &_client;
	/// A reference to the storage manager managing this column
	PlasmaStMan::impl &_storage_manager;
	/// The reader object for this column
	std::unique_ptr<ArrowReader> _reader;
	/// The shape of this column's cells
	IPosition _shape;
	/// Indicates whether the Arrow object's shape and type have been checked
	mutable bool _conformance_checked = false;

	virtual void setShapeColumn(const IPosition &aShape) override;

	/**
	 * Read data into a casacore array from the underlying Arrow object storage
	 * at a given offset. This could be a zero-copy operation depending on the
	 * type of the underlying Arrow object.
	 * @param array The array to put the Arrow data in.
	 * @param offset The offset in the Arrow object storage at which the array
	 * should start.
	 */
	void read_array(ArrayBase &array, std::size_t offset);

	/**
	 * Returns the shape of the column, including the number of rows as the
	 * first dimension
	 * @return This column's shape
	 */
	Shape get_column_shape() const;

	/// Check that the Arrow object's shape and type matches this column's
	void check_conformance() const;

public:
	/**
	 * Create a new PlasmaStManColumn with the given name and data type. Upon
	 * construction it connects to Plasma and retrieves the underlying Arrow
	 * object, if known at this stage; otherwise a call to initialize_reader
	 * needs to be issued later before attempting to read anything.
	 * @param name The name of this column.
	 * @param client The Plasma client object used to read Arrow objects off
	 * Plasma.
	 * @param storage_manager A reference to the owning storage manager, used to
	 * retrieve the number of rows after table creation.
	 * @param object_info Structure containing the Object ID and type of Arrow
	 * object to read from Plasma. If the type is ArrowObjectType::UNKNOWN then
	 * no reading occurs.
	 * @param dataType The data type of this column.
	 */
	PlasmaStManColumn(const std::string &name,
	    PlasmaClient &client,
	    PlasmaStMan::impl &storage_manager,
	    const ArrowObjectInfo &object_info, int dataType);

	/**
	 * Initializes the underlying reader object with the provided information.
	 * @param object_info Structure containing the Object ID and type of Arrow
	 * object to read from Plasma. If the type is ArrowObjectType::UNKNOWN then
	 * no initialization occurs.
	 */
	void initialize_reader(const ArrowObjectInfo &object_info);

	/**
	 * @return Whether the underlying reader is initialized or not.
	 */
	bool reader_initialized() const;

	IPosition shape(rownr_t rownr) override;

protected:

	// scalar get
	virtual void getBool(rownr_t aRowNr, Bool *aDataPtr) override;
	virtual void getuChar(rownr_t aRowNr, uChar *aDataPtr) override;
	virtual void getShort(rownr_t aRowNr, Short *aDataPtr) override;
	virtual void getuShort(rownr_t aRowNr, uShort *aDataPtr) override;
	virtual void getInt(rownr_t aRowNr, Int *aDataPtr) override;
	virtual void getuInt(rownr_t aRowNr, uInt *aDataPtr) override;
	virtual void getInt64(rownr_t aRowNr, Int64 *aDataPtr) override;
	virtual void getfloat(rownr_t aRowNr, Float *aDataPtr) override;
	virtual void getdouble(rownr_t aRowNr, Double *aDataPtr) override;
	virtual void getComplex(rownr_t aRowNr, Complex *aDataPtr) override;
	virtual void getDComplex(rownr_t aRowNr, DComplex *aDataPtr) override;

	virtual void getScalarColumnV(ArrayBase& data) override;
	virtual void getScalarColumnCellsV(const RefRows& rownrs, ArrayBase& data) override;

	virtual void getArrayV(rownr_t rownr, ArrayBase& data) override;

	virtual void getArrayColumnV(ArrayBase& data) override;
	virtual void getArrayColumnCellsV(const RefRows& rownrs, ArrayBase& data) override;

};

} // namespace plasma
} // namespace ska

#endif // SKA_PLASMA_PLASMA_STMAN_COLUMN_H