#include <cassert>
#include <memory>
#include <utility>

#include <arrow/array.h>
#include <arrow/table.h>
#include <arrow/ipc/reader.h>
#include <casacore/casa/Arrays/IPosition.h>

#include "ska/plasma/arrow_buffer_access.h"
#include "ska/plasma/exceptions.h"
#include "ska/plasma/table_reader.h"
#include "ska/plasma/types.h"

namespace ska {
namespace plasma {

TableReader::TableReader(const std::string &column_name,
  casacore::DataType data_type, arrow::io::InputStream* input_stream)
  : ArrowReader(column_name, data_type)
{
	auto batches_reader = arrow::ipc::RecordBatchStreamReader::Open(input_stream);
	if (!batches_reader.ok())
	{
		throw plasma_error("problem creating record batch reader");
	}
	std::shared_ptr<arrow::Table> table;
	auto status = batches_reader.ValueUnsafe()->ReadAll(&table);
	if (!status.ok())
	{
		throw plasma_error("problem reading table");
	}
	_table = std::move(table);
	auto arrow_column = _table->GetColumnByName(column_name);
	if (!arrow_column)
	{
		throw plasma_error("Column not found in Arrow Table");
	}
	if (arrow_column->num_chunks() != 1)
	{
		throw exception("Tables written in more than one chunk are not supported");
	}
}

const std::shared_ptr<arrow::ChunkedArray> TableReader::column() const
{
	assert(!column_name().empty());
	auto column = _table->GetColumnByName(column_name());
	assert(column);
	assert(column->num_chunks() == 1);
	return column;
}

void TableReader::check_types() const
{
	check_expected(*column()->type(), *get_table_type(casacore_type()));
}

void TableReader::check_shape(const Shape &column_shape) const
{
	Shape our_shape {column()->length()};
	check_expected(our_shape, column_shape);
}

template <typename CasacoreType>
void TableReader::read_scalar_impl(rownr_t rownr, void *dataPtr)
{
	// We can assume the column exists at this point, as this is checked earlier
	auto first_array = column()->chunk(0);
	auto *parray = dynamic_cast<arrow::PrimitiveArray *>(first_array.get());
	read_scalar_from_buffer<CasacoreType>(parray->values(), rownr, dataPtr);
}

std::pair<arrow::PrimitiveArray *, arrow::PrimitiveArray *>
static get_complex_field_arrays(std::shared_ptr<arrow::Array> &&array)
{
	auto *struct_array = dynamic_cast<arrow::StructArray *>(array.get());
	auto real_array = struct_array->GetFieldByName("r");
	auto imag_array = struct_array->GetFieldByName("i");
	return {
		dynamic_cast<arrow::PrimitiveArray *>(real_array.get()),
		dynamic_cast<arrow::PrimitiveArray *>(imag_array.get())
	};
}

template <typename ComplexT>
void TableReader::read_complex_scalar(rownr_t rownr, void *dataPtr)
{
	using value_type = typename ComplexT::value_type;
	arrow::PrimitiveArray *real = nullptr;
	arrow::PrimitiveArray *imag = nullptr;
	std::tie(real, imag) = get_complex_field_arrays(column()->chunk(0));
	auto *userDataAsFloat = static_cast<value_type *>(dataPtr);
	read_scalar_from_buffer<value_type>(real->values(), rownr, userDataAsFloat);
	read_scalar_from_buffer<value_type>(imag->values(), rownr, userDataAsFloat + 1); // NOLINT
}

template <>
void TableReader::read_scalar_impl<Complex>(rownr_t rownr, void *dataPtr)
{
	read_complex_scalar<Complex>(rownr, dataPtr);
}

template <>
void TableReader::read_scalar_impl<DComplex>(rownr_t rownr, void *dataPtr)
{
	read_complex_scalar<DComplex>(rownr, dataPtr);
}

void TableReader::read_scalar(rownr_t rownr, void *dataPtr)
{
	INVOKE_FOR_DATATYPE(
		casacore_type(),
		read_scalar_impl,
		rownr, dataPtr
	);
}

template <typename CasacoreType>
void TableReader::read_array_impl(ArrayBase &array, std::size_t offset)
{
	auto first_array = column()->chunk(0);
	auto *parray = dynamic_cast<arrow::PrimitiveArray *>(first_array.get());
	read_array_from_buffer<CasacoreType>(parray->values(), offset, array);
}

template <typename ComplexT>
void TableReader::read_complex_array(ArrayBase &array, std::size_t offset)
{
	using Slice = casacore::Slice;
	arrow::PrimitiveArray *arrow_real = nullptr;
	arrow::PrimitiveArray *arrow_imag = nullptr;
	std::tie(arrow_real, arrow_imag) = get_complex_field_arrays(column()->chunk(0));

	// Reinterpret the complex Array as a Nx2 simple_type array, then interpret
	// its columns as individual 1D arrays
	using simple_type = typename ComplexT::value_type;
	auto *array_with_complex_type = static_cast<casacore::Array<ComplexT> *>(&array);
	auto nelements = array.shape()[0];
	auto simple_array = casacore::Array<simple_type>(
		IPosition{2, nelements},
		reinterpret_cast<simple_type *>(array_with_complex_type->data()), // NOLINT
		casacore::StorageInitPolicy::SHARE
	);
	auto real_array = simple_array(
		Slicer{
			Slice{0, 1},
			Slice{0, static_cast<std::size_t>(nelements)},
		}
	);
	auto imag_array = simple_array(
		Slicer{
			Slice{1, 1},
			Slice{0, static_cast<std::size_t>(nelements)},
		}
	);
	read_array_from_buffer<simple_type>(arrow_real->values(), offset, real_array);
	read_array_from_buffer<simple_type>(arrow_imag->values(), offset, imag_array);
}

template <>
void TableReader::read_array_impl<Complex>(ArrayBase &array, std::size_t offset)
{
	read_complex_array<Complex>(array, offset);
}

template <>
void TableReader::read_array_impl<DComplex>(ArrayBase &array, std::size_t offset)
{
	read_complex_array<DComplex>(array, offset);
}

void TableReader::read_array(ArrayBase &array, std::size_t offset)
{
	INVOKE_FOR_DATATYPE(
		casacore_type(),
		read_array_impl,
		array, offset
	);
}



}  // namespace plasma
}  // namespace ska